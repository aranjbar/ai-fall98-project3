import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt
import time

if __name__ == '__main__':
    start = time.time()
    data = pd.read_csv('./data/train_test.csv', dtype='str', delimiter=',')
    # dic = data.to_dict(orient='records')
    # data = np.loadtxt('./data/train_test.csv', dtype='str', delimiter=',')
    # data = np.char.split(data, sep=' ')
    data['text'] = data['text'].str.split(' ')
    train = data.sample(frac=0.8)
    test = data.drop(train.index)
    dic = {}
    for _, d in train.iterrows():
        try:
            dic[d['label']].append((d['text']))
        except KeyError:
            dic[d['label']] = [d['text']]
    hafez = {'hemistich': dic['hafez'], 'word': sum(dic['hafez'], [])}
    hafez['count'] = Counter(hafez['word'])
    hafez['hemistich_count'] = len(hafez['hemistich'])
    hafez['word_count'] = len(hafez['word'])
    hafez['hemistich_length'] = [len(h) for h in hafez['hemistich']]

    saadi = {'hemistich': dic['saadi'], 'word': sum(dic['saadi'], [])}
    saadi['count'] = Counter(saadi['word'])
    saadi['hemistich_count'] = len(saadi['hemistich'])
    saadi['word_count'] = len(saadi['word'])
    saadi['hemistich_length'] = [len(h) for h in saadi['hemistich']]

    train_end = time.time()
    print('train time: {}'.format(train_end - start))
    # plt.hist(saadi['hemistich_length'], bins='auto')
    # plt.show()
    # plt.hist(hafez['hemistich_length'], bins='auto')
    # plt.show()
    # Test
    correct = 0
    for _, hemistich in test.iterrows():
        p_saadi = 1
        for word in hemistich['text']:
            try:
                p_saadi = p_saadi * saadi['count'][word]/saadi['word_count']
            except KeyError:
                pass
        p_hafez = 1
        for word in hemistich['text']:
            try:
                p_hafez = p_hafez * hafez['count'][word] / hafez['word_count']
            except KeyError:
                pass
        # p_saadi = p_saadi * saadi['hemistich_count']/(saadi['hemistich_count'] + hafez['hemistich_count'])
        # p_hafez = p_hafez * hafez['hemistich_count'] / (saadi['hemistich_count'] + hafez['hemistich_count'])
        if p_saadi >= p_hafez and hemistich['label'] == 'saadi':
            correct = correct + 1
        if p_hafez > p_saadi and hemistich['label'] == 'hafez':
            correct = correct + 1
    print('Accuracy: {}'.format(correct/len(test.index)*100))
    test_end = time.time()
    print('Test time: {}'.format(test_end - train_end))
